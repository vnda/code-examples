# CONTROLE DE VIDEOS NO PRODUCT-BLOCK

Movimente os trechos indicados deste repositório para o seu projeto conforme as orientações abaixo:

**Liquids**: cada versão do `product_block` contém os *trechos* de código necessários e comentários para orientar a implementação. Adapte ao contexto necessário em seu projeto.

**Scripts**: no arquivo `utilities.js` há uma função para cada uma das maneiras de execução dos videos. Utilize a que melhor se adapta à sua necessidade.