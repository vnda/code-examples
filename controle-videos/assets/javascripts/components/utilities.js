// Não esqueça de instalar a API do vimeo, caso esse seja o player utilizado
// npm install @vimeo/player
import Player from '@vimeo/player';

// ===============================================================
// CONTROLE DE VÍDEOS - APARIÇÃO EM TELA
// ===============================================================

// Ativa o play dos videos conforme a aparição dos mesmos em tela
// Caso saia da tela, o pause é ativado
export function controlVideosInViewport() {
  const videos = document.querySelectorAll('iframe[data-video-control]');
  if (videos.length === 0) return;

  let videoIndex = 0; // Índice para delay

  const observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
      const video = entry.target;

      const videoSrc = video.getAttribute('data-src');
      if (!videoSrc) return;

      // Verifica se o vídeo está ou não em tela
      if (entry.isIntersecting) {

        // Verifica se o vídeo tem o 'src' carregado
        if (!video.hasAttribute('data-iframe-loaded') && video.hasAttribute('data-src')) {
          // Calcula o delay considerando o índice
          const delay = 750 + (videoIndex * 200);

          // Limita o delay em 1200ms
          const adjustedDelay = Math.min(delay, 1200);

          // Adiciona o 'lazy' após o delay
          setTimeout(() => {
            video.setAttribute('loading', 'lazy');
            video.classList.add('lazy');
            video.setAttribute('data-iframe-loaded', 'true');

            window.lazyLoad && window.lazyLoad.update();

            // Controle para Vimeo
            if (videoSrc.includes('vimeo')) {
              // Verifica se já existe o player
              let player = video.vimeoPlayer;

              if (!player) {
                // Cria o player apenas uma vez e guarda no iframe
                player = new Player(video);
                video.vimeoPlayer = player;

                player.on('loaded', function () {
                  player.play().catch(error => {
                    console.error('Erro ao tentar executar o vídeo Vimeo:', error);
                  });
                });
              }

            } else if (videoSrc.includes('youtube')) {
              video.contentWindow && video.contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
            }

          }, adjustedDelay);

          // Incrementa o índice e reseta após 4 vídeos
          videoIndex = (videoIndex + 1) % 4;

        } else {
          // Se já está carregado, executa o play
          if (videoSrc.includes('vimeo')) {
            const player = video.vimeoPlayer;

            if (player) {
              player.play().catch(error => {
                console.error('Erro ao tentar executar o vídeo Vimeo:', error);
              });
            }

          } else if (videoSrc.includes('youtube')) {
            video.contentWindow && video.contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
          }
        }

      } else {
        // Controle para quando o vídeo sai da tela
        if (videoSrc.includes('vimeo')) {
          const player = video.vimeoPlayer;

          if (player) {
            player.pause().catch(error => {
              console.error('Erro ao tentar pausar o vídeo Vimeo:', error);
            });
          }

        } else if (videoSrc.includes('youtube')) {
          video.contentWindow && video.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
        }

      }
    });
  }, { threshold: 0.1 });

  // Observa todos os vídeos
  videos.forEach(video => observer.observe(video));
}

// ===============================================================
// CONTROLE DE VÍDEOS - APARIÇÃO CONFORME O HOVER
// ===============================================================

// O vídeo será exibido e executado conforme o hover no product-block
// Conforme o hover for cancelado, o video é pausado
export function controlVideosOnHover() {
  const videos = document.querySelectorAll('iframe[data-video-control]');
  if (videos.length === 0) return;

  videos.forEach(video => {
    const imagesContainer = video.closest(".images");

    const videoSrc = video.getAttribute('data-src');
    if (!videoSrc) return;

    let player = null;
    let hoverTimeout = null;
    const hoverDelay = 250; // Tempo necessário de hover para executar o video

    const loadAndPlayVideo = () => {
      if (!video.hasAttribute('data-iframe-loaded')) {
        video.setAttribute('loading', 'lazy');
        video.classList.add('lazy');
        video.setAttribute('data-iframe-loaded', 'true');

        if (window.lazyLoad) {
          window.lazyLoad.update();
        }

        if (videoSrc.includes('vimeo')) {
          player = new Player(video);
          video.vimeoPlayer = player;

          player.on('loaded', () => {
            player.play().catch(error => {
              console.error('Erro ao tentar executar o vídeo Vimeo:', error);
            });
          });
        } else if (videoSrc.includes('youtube')) {
          video.src = videoSrc;
          video.contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
        }
      } else {
        // Video já carregado
        if (videoSrc.includes('vimeo') && player) {
          player.play().catch(error => {
            console.error('Erro ao tentar executar o vídeo Vimeo:', error);
          });
        } else if (videoSrc.includes('youtube')) {
          video.contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
        }
      }
    };

    const pauseVideo = () => {
      clearTimeout(hoverTimeout); // Limpa o timeout se o hover sair antes do tempo do delay
      if (videoSrc.includes('vimeo') && player) {
        player.pause().catch(error => {
          console.error('Erro ao tentar pausar o vídeo Vimeo:', error);
        });
      } else if (videoSrc.includes('youtube')) {
        video.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
      }
    };

    if (imagesContainer) {
      imagesContainer.addEventListener('mouseenter', () => {
        hoverTimeout = setTimeout(loadAndPlayVideo, hoverDelay);
      });

      imagesContainer.addEventListener('mouseleave', pauseVideo);
    }

  });
}