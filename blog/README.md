# BLOG

Movimente os arquivos deste repositório para o seu projeto conforme as orientações abaixo:

**Liquids**: Adicione a pasta `media` para dentro de `liquids`;  
**Estilos**: Adicione a pasta `blog`, que está em `assets/sass` para dentro de `assets/sass/pages`;  
**Scripts**:
- Adicione o arquivo `blog.js` para dentro de `assets/javascripts/pages`;
- Adicione o arquivo `infinityScroll.js` para dentro de `assets/javascripts/components`. Caso o mesmo já exista, atualize-o apenas;

&nbsp;

## ORIENTAÇÕES PARA ABERTURA E CONFIGURAÇÃO INICIAL DO BLOG

Acessar as instruções em [Developers](https://developers.vnda.com.br/docs/blog);

Dúvidas, enviar para `ajuda@vnda.com.br`;

&nbsp;

## ESTRUTURA DOS POSTS

***Informações:***

| Dúvida                          | Instrução                                                                            |
| ------------------------------- | ------------------------------------------------------------------------------------ |
| **Onde cadastrar**              | Cockpit                                                                              |
| **Onde será exibido**           | Configurações de cadastro dos posts do blog                                          |

&nbsp;

***Importante***

Para utilizar o modelo base do blog, será necessário criar e configurar alguns campos no cockpit.

No `Content`, acessar a `Collection` - este modelo utiliza a collection com nome `posts` - e seguir as orientações abaixo:

&nbsp;

***Orientações sobre os campos:***

| Campo                  | Tipo         | Label              | Required  | Orientação                                              |
| ---------------------- | ------------ | ------------------ | --------- | ------------------------------------------------------- |
| **title**              | `Text`       | Título             | Sim       | Título do post                                          |
| **description**        | `Textarea`   | Descrição          | Sim       | Descrição curta do post                                 |
| **thumb**              | `Image`      | Thumb              | Sim       | Imagem exibida na listagem de posts. Dimensões sugeridas: 400x400 pixels |
| **author**             | `Text`       | Autor              | Não       | Nome do autor - Opcional                                |
| **date**               | `Date`       | Data               | Sim       | Data do post                                            |
| **fullbanner_desktop** | `Image`      | Fullbanner Desktop | Sim       | Banner principal. Dimensões sugeridas: 1920x890 pixels  |
| **fullbanner_mobile**  | `Image`      | Fullbanner Mobile  | Sim       | Banner principal. Dimensões sugeridas: 992x1450 pixels  |
| **content**            | `Wysiwyg`    | Conteúdo           | Sim       | Conteúdo do post em si. Insira textos e imagens utilizando o editor do Cockpit. Ver detalhes sobre inserção de imagens abaixo |
| **categories**         | `Tags`       | Categorias         | Não       | Adicionar as Categorias que o post pertence - Essas, serão exibidas para filtragem na página inicial do Blog |
| **relateds**           | `Text`       | Posts Relacionados | Não       | Inserir termo em comum para os posts relacionados. Ex.: `posts-relacionados`, todos os posts que tiverem no seu campo Posts Relacionados, o mesmo termo, serão exibidos como sugestão de leitura |
| **publish**            | `Boolean`    | Publicar           | Sim       | Marcar se o post deve ser exibido ou não                  |
| **permalink**          | `Text`       | Permalink          | Não       | Inserir URL amigável do post. Será implementado no futuro |

&nbsp;

***Adicionando imagens em Thumbs, Fullbanner Desktop, Fullbanner Mobile e em Conteúdo***
- Utilize sempre a opção *Assets*
- Ex.: Na Thumb, em Source, escolha a opção *Select Asset*
- Ex.: No Conteúdo, utilize a opção *Insert > Insert Assets (Assets)*
- As dimensões para a inserção de imagens dentro do conteúdo são livres

&nbsp;

***Adicionando Vídeos no Conteúdo***
- Vá em `Insert`, logo após selecione `Media`. No campo `Source` adicione a `URL` do vídeo e clique em OK

&nbsp;

***
