// ===============================================================
// Retorna os dados do cliente
// ===============================================================
export async function getClient() {
    try {
      const response = await fetch('/conta/cliente', {
        headers: {
          'accept': 'application/json',
          'Content-Type': 'application/json',
          'cache': 'no-store'
        }
      });
      const client = await response.json();
      const access = Object.keys(client).length > 0;
  
      if (access) {
        window.client = client;
        
        return client
  
      } else {
  
        return null
      }
  
  
    } catch (error) { console.error(`getClient error`, error) }
  }