# DESCRIÇÃO COM ABAS

Ajuste os arquivos do seu projeto conforme as orientações abaixo:

**Liquid**: Adicione o arquivo `description.liquid` que está em `liquids` para dentro de `liquids/partials/content/product`.

**Estilo**: Adicione o arquivo `description.scss` que está em `assets/sass` para dentro de `assets/sass/content/product`.

**Script**: Modifique o arquivo `product.js` com base no que está em `assets/javascripts`, adicionando a função `setDescription`.

&nbsp;

***Como funciona:***

- Responsável pela loja cadastra descrição com abas nos produtos desejados, seguindo o modelo previsto;
- Cliente acessa um dos produtos, exibindo a primeira aba de forma expandida;
- Ao selecionar uma das abas, a aba expandida anteriormente é reduzida, expandindo a aba selecionada.

&nbsp;

***Importante:***

- Com esse modelo será necessário cadastrar a descrição com abas em produtos no painel administrativo da loja acessando `/admin/produtos`;
- Acesse o painel administrativo da loja e cadastre a descrição de um produto, seguindo o modelo de collapses com títulos/textos exibidos em cada aba;
- Produtos com a descrição no modelo previsto exibirão as abas expansivas na página interna;
- Produtos com a descrição no modelo padrão (sem collapses) exibirão uma única aba com o título `Descrição`, contendo o texto inserido no cadastro.

&nbsp;

***Exemplo de uso:***

- Produto com descrição com abas: [Página](https://desnude.vnda.dev/produto/jumpsuit-bianca-1) / [Admin](https://desnude.vnda.dev/admin/produtos/editar?id=1)

&nbsp;

## PRODUTO

***Informações:***

| Dúvida                          | Instrução                   |
| ------------------------------- | --------------------------- |
| **Onde cadastrar**              | Produtos `/admin/produtos`  |
| **Onde será exibido**           | Página interna do produto   |

***Informações sobre os campos***

| Campo          | Funcional?          | Orientação                                          |
| -------------- | ------------------- | --------------------------------------------------- |
| **Imagens**    | :no_entry:          |                                                     |
| **Referência** | :black_circle:      | Obrigatório no admin, sem reflexo na implementação. |
| **Nome**       | :black_circle:      | Obrigatório no admin, sem reflexo na implementação. |
| **Descrição**  | :large_blue_circle: | Título e texto das abas, separados por collapses `---`. Siga o exemplo de Markdown! |
| **Tags**       | :no_entry:          |                                                     |
| **Variantes**  | :no_entry:          |                                                     |

**Ex.: Markdown Descrição**

```
### Descrição I

Lorem ipsum dolor sit amet.

Arcu habitant nunc aliquam ac egestas id. Viverra id tortor sagittis, non leo, quam neque inter dum. Aenean morbi aliquet leo rhoncus. Vesti bulum urna, dolor nisi, magna mollis trist ique dignissim odio sed. Amet interdum a tortor odio.

---

### Descrição II

Lorem ipsum dolor sit amet, cons ectetur adip iscing elit. Habitant ullam corper mi a tincidunt nec, amet nec diam. Hac nunc et accu msan vel lacus, ac facilisis lorem.

---

### Descrição III

Venenatis, pretium a sodales et lorem scele risque. Odio nibh sit mau ris ma gna. Arcu habitant nunc aliquam ac egestas id. Viverra id tortor sagittis, non leo, quam neque inter dum.

---

### Descrição IV

Aenean morbi aliquet leo rhoncus. Vesti bulum urna, dolor nisi, magna mollis trist ique dignissim odio sed. Amet interdum a tortor odio.
```

&nbsp;

***