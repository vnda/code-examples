const Product = {

  setDescription: function () {
    const description = document.querySelector('[data-description]');
    if (description) {
      let anchors = description.querySelectorAll('.anchor');
      let dropdowns = description.querySelectorAll('.dropdown');

      if (anchors.length > 0 && dropdowns.length > 0)
        anchors.forEach((anchor, i) => {
          anchor.addEventListener('click', () => {
            
            if (anchor.classList.contains('-active')) {
              anchors[i].classList.remove('-active');
              dropdowns[i].classList.remove('-active');

            } else {
              anchors.forEach(a => { a.classList.remove('-active') });
              dropdowns.forEach(d => { d.classList.remove('-active') });

              anchors[i].classList.add('-active');
              dropdowns[i].classList.add('-active');
            }
          });
        });
    }
  },

  init: function () {
    this.setDescription();
  },
};

window.addEventListener('DOMContentLoaded', () => {
  Product.init();
});
