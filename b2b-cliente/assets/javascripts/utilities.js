// ===============================================================
// CLIENT DATA
// ===============================================================
export async function getClient() {
  try {
    let response = await fetch('/conta/cliente', {
      headers: { 
        accept: 'application/json'
      }
    });
    let client = await response.json();
    let access = Object.keys(client).length > 0;
    
    console.info('client', client);
    console.info('client.tags', client.tags);

    if (access)
      return client;
    if (!access)
      return false;
    
  } catch (error) { console.error(`getClient error`, error) }
}