# B2B CLIENTE

Ajuste os arquivos do seu projeto conforme as orientações abaixo:

**Scripts**

- Modifique o arquivo `utilities.js` do seu projeto adicionando a função que está em `assets/javascripts`;
- Uma vez que exportada, utilize a função para verificar dados do cliente e manipular o conteúdo das páginas.

&nbsp;

***Como funciona:***

- Após cadastrado na plataforma, o cliente receberá tags responsáveis pela exibição de conteúdos nas páginas, vinculadas manualmente através do painel;
- O controle de conteúdo com base nas tags do cliente deve ser previsto através do código do projeto verificando `client.tags`;
- Dentre as modificações de conteúdo, algumas das opções mais comuns nas lojas são exibição de preço nos produtos, permissão de adição ao carrinho, acesso a página interna do produto, etc.

&nbsp;

***Exemplos de uso:***

- Produto com conteúdo oculto: [Página](https://ethnos.vnda.dev/produto/c16-pe-de-fibra-de-carbono-novo-foot-lorem-ipsum-1) / [Preview](https://i.imgur.com/wb3v3r9.png)
  - Visualização prevista para cliente sem tag `acesso-liberado`
- Produto com conteúdo visível: [Página](https://ethnos.vnda.dev/produto/c16-pe-de-fibra-de-carbono-novo-foot-lorem-ipsum-1) / [Preview](https://i.imgur.com/7Acrw7v.png)
  - Visualização prevista para cliente com tag `acesso-liberado`
- Cliente com tag `acesso-liberado`: [Admin](https://ethnos.vnda.dev/admin/clientes/editar?id=19)
  - E-mail: teste@vnda.com.br / Senha: testevnda

&nbsp;

## CLIENTES

***Informações:***

| Dúvida                          | Instrução                                         |
| ------------------------------- | ------------------------------------------------- |
| **Onde cadastrar**              | Clientes `/admin/clientes`                        |
| **Onde será exibido**           | Todas as páginas (conforme os requisitos da loja) |

***Orientações sobre os campos:***

| Campo         | Funcional?     | Orientação |
| ------------- | -------------- | ---------- |
| **Nome**      | :black_circle: | Obrigatório no admin, sem reflexo na página. |
| **Sobrenome** | :black_circle: | Obrigatório no admin, sem reflexo na página. |
| **E-mail**    | :black_circle: | Obrigatório no admin, sem reflexo na página. |
| **Tags**      | :black_circle: | Tags responsáveis por modificar o conteúdo das páginas conforme o que estiver previsto no código do projeto. |

&nbsp;

***