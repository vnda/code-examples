// ===============================================================
// LANDING PAGE CONTROL
// ===============================================================
export function landingPage() {
  const landingPage = document.querySelector('[data-landing-page]');
  if (landingPage) {
    
    let access =
      typeof localStorage !== 'undefined' && localStorage.getItem('access')
        ? true
        : window.location.search.includes('access')
          ? true
          : false
    
    if (access) {
      if (typeof localStorage !== 'undefined' && !localStorage.getItem('access'))
        localStorage.setItem('access', true);

      landingPage.classList.add('-hidden');
    } else {
      landingPage.classList.add('-visible');
    }
  }
}