import { getCart } from "./components/utilities";
import Store from './components/store'

const LocalsPopup = {
  popup: document.querySelector('[data-popup-local]'),
  navigationAllowed: true, // Permite que o usuário navegue pelo site sem o local definido

  // Controla o popup de seleção de locais
  handleLocalsPopup: function () {
    const { popup, navigationAllowed, setSessionStorageItems } = LocalsPopup;
    if (!popup) return;

    const selectedCEP = popup.querySelector('[data-modal-cep]');

    // Se o local não foi definido e a navegação não for permitida, exibe o popup
    if ((!sessionStorage.getItem('local-cep') || !window.currentLocal) && !sessionStorage.getItem('local-navigation-allowed')) {
      popup.classList.add('-active');
      selectedCEP.innerHTML = "";
    }

    // Exibe CEP atual, caso o mesmo já tenha sido definido
    if (sessionStorage.getItem('local-cep') && window.currentLocal) {
      const storedItem = JSON.parse(sessionStorage.getItem('local-cep'));
      const zipInput = document.querySelector(".modal-input");
      zipInput && zipInput.setAttribute("placeholder", `CEP atual: ${storedItem}`);
    }

    // Exibe CEP atual, caso o mesmo já tenha sido definido
    if (window.currentLocal && window.currentLocal.name) {
      const localsDisplayName = document.querySelectorAll('.local-name');
      localsDisplayName.length > 0 && localsDisplayName.forEach(local => {

        const name = local.querySelector('[data-user-local]');
        if (name) name.textContent = window.currentLocal.name;

        local.classList.remove('-hidden')
      })
    }

    const openers = document.querySelectorAll("[data-show-locals-popup]");
    openers.length > 0 && openers.forEach(opener => {
      opener.addEventListener("click", () => {
        popup.classList.add("-active");
      });
    });

    const closers = document.querySelectorAll("[data-close-locals-popup]");
    closers.length > 0 && closers.forEach(button => {

      // Exibe os elementos que habilitam a navegação do site
      if (navigationAllowed) button.classList.remove('-hidden');

      button.addEventListener('click', () => {

        // Se o local já foi definido ou a navegação é permitida
        if (window.currentLocal || navigationAllowed) {
          popup.classList.remove('-active');
        }

        // Se a navegação é permitida e não há o dado no sessionStorage
        // Adiciona para não mostrar o popup novamente no carregamento
        if (navigationAllowed && !sessionStorage.getItem('local-navigation-allowed')) {
          setSessionStorageItems('local-navigation-allowed', true);
        }

      })
    });
  },

  // Define o local selecionado
  submitLocal: function () {
    const { popup, clearCart, setSessionStorageItems } = LocalsPopup;
    if (!popup) return;

    const form = popup.querySelector('form');
    const submitButton = form.querySelector('.button-default')

    form && form.addEventListener('submit', async (e) => {
      e.preventDefault();

      submitButton.innerHTML = 'VERIFICANDO...';

      const data = new FormData(e.target);
      const url = e.target.action;
      const elmResponse = e.target.querySelector('.response');

      fetch(url, {
        method: 'POST',
        body: data,
      })
        .then(response => response.json())
        .then(json => {
          console.log('Locais ->', json);

          if (json.serves) {
            setSessionStorageItems("local-cep", data.get('zip'))
            setSessionStorageItems("user-selected-local", json)

            // Limpa o carrinho após a troca de CEP
            clearCart();

            const newUrl = window.location.href.includes('refresh')
              ? window.location.href.split('refresh')[0] + `refresh=${Date.now()}`
              : (window.location.href.includes('?')
                ? window.location.href += `&refresh=${Date.now()}`
                : window.location.href += `?refresh=${Date.now()}`);

            setTimeout(() => {
              window.location.href = newUrl;
            }, 1000)

          } else {

            // Mensagem de pós envio
            elmResponse.innerHTML = 'Nenhuma forma de entrega foi encontrada para o CEP digitado';

            setTimeout(() => {
              elmResponse.classList.add('-hidden');
            }, 3000)

            setTimeout(() => {
              elmResponse.innerHTML = "";
              elmResponse.classList.remove('-hidden');
            }, 3500)

          }
        })
        .catch(error => { elmResponse.innerHTML = `${error.message}`; })
        .finally(() => { submitButton.innerHTML = 'CONFIRMAR' });
    });
  },

  // Limpa o carrinho na troca de locais ou quando a sessão expira
  clearCart: async function () {
    const cart = await getCart();
    const cartDrawer = document.getElementById("component-cart-drawer-root");

    // Valida se há itens no carrinho
    if (cart && cart.items.length > 0) {
      for (const item of cart.items) {
        const itemId = item.id
        await Store.deleteItem(itemId, cartDrawer);
      }
    }
  },

  // Define os dados do local e CEP digitado no sessionStorage
  setSessionStorageItems: function (key, value) {
    sessionStorage.setItem(key, JSON.stringify(value));
  },

  init: function () {
    const { handleLocalsPopup, submitLocal, clearCart } = LocalsPopup;

    // Limpa o carrinho caso a sessão tenha expirado
    if (!sessionStorage.getItem('user-selected-local')) {
      clearCart();
    } else {
      // Atualiza o local selecionado
      window.currentLocal = JSON.parse(sessionStorage.getItem('user-selected-local'));
    }

    const eventType = (window.innerWidth <= 1024) ? 'scroll' : 'mousemove'
    window.addEventListener(eventType, () => {
      handleLocalsPopup();
      submitLocal();
    }, { once: true });

  },
}

LocalsPopup.init()