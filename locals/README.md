# LOCAIS

&nbsp;

## Antes de iniciar

As instruções deste repositório ajudam a implementar um Popup para a seleção de Locais nas lojas que já possuem o `productPurchase.js` configurado para controlar produtos vendidos com base em Locais. Os templates atuais já estão preparados para tal funcionalidade.

Para verificar se sua loja já está configurada para utilizar Locais, procure pela propriedade `useLocals` no arquivo `productPurchase.js` do seu projeto. Se a propriedade existir, o código da sua loja já possui a estrutura necessária.

&nbsp;

## Configurações dos Locais/Multi CD

Para disponibilizar Multiplos estoques para venda, algumas configurações no Painel da loja serão necessárias:
[Orientações](http://ajuda.vnda.com.br/pt-BR/articles/9018977-regras-de-multi-cd?q=Multi+CD)

&nbsp;

## Arquivos

Movimente os arquivos indicados deste repositório para o seu projeto conforme as orientações abaixo:

**Liquids**:

- `locals_popup.liquid`: deve ser inserido na pasta `liquids/partials/common/` do projeto e renderizado no arquivo `layout.liquid`, dentro da tag `<body>` do HTML conforme modelo:

```liquid
{% render "partials/common/locals_popup" %}
```

- `actions.liquid`: neste arquivo há o trecho de código do botão que **ativa** o popup de inserção de CEP. A sugestão de utilização é que seja adicionado no header, junto dos demais elementos do `actions.liquid` como Busca, Login, Carrinho etc. Entretanto pode ser inserido onde fizer mais sentido para a loja. Modelo:

```liquid
<div class="actions">
  <!-- Restante do código ... -->

  <button class="select-local" type="button" aria-label="Abrir popup de seleção de locais" data-show-locals-popup >
    Selecionar Locais
  </button>

  <!-- Restante do código ... -->
</div>
```

**Estilos**:

- `locals_popup.scss`: esse arquivo possui as configurações visuais base para o popup de inserção de CEP. Deve ser inserido na pasta `assets/sass/common/` do projeto e importado no arquivo `style.scss` conforme modelo:

```scss
@import "common/locals_popup"
```

**Scripts**:

- `localsPopup.js`: arquivo que manipula o envio do CEP digitado pelo usuário e realiza as configurações necessárias relacionadas ao Local retornado e navegação pelo site. Deve ser inserido no caminho `assets/javascripts/` e chamado no arquivo `layout.liquid`, antes do arquivo `main.js`, conforme modelo:

```liquid

// ... Restante do código

<script src="{{ 'jquery.min.js' | javascript_path }}"></script>
<script src="{{ 'localsPopup.js' | javascript_path }}"></script>
<script src="{{ 'main.js' | javascript_path }}"></script>

{{ content_for_body }}

// ... Restante do código

```

&nbsp;

## Importante

- Para realizar testes no ambiente de staging, é necessário solicitar ao time de Atendimento a configuração dos Locais no painel da loja, bem como formas de entrega vinculadas aos locais cadastrados.
**Importante**: Não será possível selecionar locais se não houver formas de entrega vinculadas a eles.

- Quando um CEP é enviado pelo usuário através do endpoint `cep/choose-local`, o sistema retorna os locais disponíveis ou nenhum local encontrado (quando a região não é atendida por nenhuma forma de entrega/local).

- Para que os locais sejam exibidos corretamente no front-end da loja e funcionem conforme esperado, os locais cadastrados no painel devem ter as caixas "Local atua como Centro de Distribuição" e "Listar local no ecommerce" selecionadas.

- As variantes dos produtos precisam ter seus estoques e preços configurados adequadamente conforme o [modelo](https://prnt.sc/dhJRikvBEuw6).

- No arquivo `productPurchase.js` há um [trecho](https://prnt.sc/eUFHnmSD-sR_) onde é necessário alterar o valor de `useLocals` para `true`, para habilitar a funcionalidade no front-end;

- No arquivo `localsPopup.js`, há um [trecho](https://prnt.sc/giD9MlrBrcaD) onde é possível habilitar/desabilitar a navegação pelo site sem a inserção de CEP.

- Os dados do Local selecionado e CEP recebido ficam armazenados no `SessionStorage` para melhor controle de cache. Quando a sessão expira, o popup é exibido e o CEP é solicitado novamente.

&nbsp;