const UploadImage = {
  // Dados do Cloudinary
  config: {
    cloudName: 'INFORMACAO_NO_CLOUDINARY',
    uploadPreset: 'INFORMACAO_NO_CLOUDINARY',
    maxSize: 10 * 1024 * 1024, // 10MB
    allowedTypes: [
      'image/png',
      'image/jpg',
      'image/webp'
    ]
  },

  // Validação do tipo do arquivo
  validateFileType: function (file) {
    if (!this.config.allowedTypes.includes(file.type)) {
      console.error('Formato não permitido do arquivo: ', file)
      file.value = ''; // Clear the selected file
      return false
    }
    console.log('Formato do arquivo permitido: ', file)
    return true
  },

  validateFileSize: function (file) {
    if (file.size > this.config.maxSize) {
      console.error('Tamanho maximo do arquivo permitido é 10MB');
      file.value = ''; // Clear the selected file
      return false
    } else {
      console.log('Tamanho do arquivo permitido: ', file.size)
      return true
    }
  },

  handleImages: function () {
    console.log('handle images');

    const customizationImage = document.querySelector('[data-customization-file]')
    const customizationFileName = document.querySelector('[data-customization-file-name]')
    const customizationError = document.querySelector('[data-customization-error]');

    if (!customizationImage) return;

    customizationImage.addEventListener('change', (e) => {
      const file = e.target.files[0]

      if (!file) return;

      const name = file.name || 'Nenhum arquivo selecionado'
      const validFileType = UploadImage.validateFileType(file);
      const validFileSize = UploadImage.validateFileSize(file);

      // Quando o formato do arquivo for válido
      if (validFileType && validFileSize) {
        window.fileToUpload = file;
        customizationFileName.textContent = name;
      } else if (!validFileType) {
        customizationError.textContent = 'Formato inválido! Envie arquivos nos formatos permitidos'
        setTimeout(() => {
          customizationError.textContent = ''
        }, 3000)
      } else if (!validFileSize) {
        customizationError.textContent = 'Tamanho máximo permitido excedido, os arquivos deve ter no maximo 10mb'
        setTimeout(() => {
          customizationError.textContent = ''
        }, 3000)
      }


    })
  },

  handleUpload: async function (file, customizationInput) {
    try {
      ///////////////////////////////////////////
      //// Implementação com Cloudinary
      ///////////////////////////////////////////

      const formData = new FormData();
      formData.append('file', file);
      formData.append('upload_preset', this.config.uploadPreset);
      formData.append('folder', 'user_uploads'); // Configurado no Cloudinary
      formData.append('tags', 'web_upload'); // Para melhor gerenciamento
      formData.append('context', `filename=${encodeURIComponent(file.name)}`);

      const response = await fetch(
        `https://api.cloudinary.com/v1_1/${this.config.cloudName}/image/upload`,
        {
          method: 'POST',
          body: formData,
          referrerPolicy: 'no-referrer' // Header de segurança
        }
      );

      if (!response.ok) {
        const error = await (response.json());
        throw new Error(error.message || 'Erro no upload');
      }

      const data = await response.json();

      console.log('link gerado: ', data.secure_url);

      // Insere o link para a imagem no Cloudinary no input da customização
      // É necessário remover o 'disabled' do atributo, caso ele esteja disabled o input não é enviado no form
      customizationInput.value = data.secure_url;
      customizationInput.removeAttribute('disabled');

      return data.secure_url

    } catch (error) {
      console.error('Erro no upload: ', error);
      this.showError(error.message);
    }
  }
}

export default UploadImage;
