# UPLOAD DE IMAGENS

Movimente os trechos indicados deste repositório para o seu projeto conforme as orientações abaixo:

**Scripts**:
- Arquivos alterados/criados:
  - `store.js` alterado
    - Validação e upload no `addItem`
  - `customizations.js`alterado
    - Adiciona o listener no input type file
  - `uploadImages` adicionado
    - Faz todo o processo relacionado ao envio e validação das imagens

**Liquid**:
- Arquivos alterados/criados:
  - `layout.liquid`
  - `_customizations.liquid` alterado em `liquids / partials / content / product / customizations `
  - Copie o aqruivo `_images.liquid` e cole em `liquids / partials / content / product / customizations`

**CSS**:
- Arquivos alterados/criados:
  - `attributos.scss` alterado em `assets / sass / content / product / infos`

## Como ativar e usar

- O processo efetua o upload de imagens para o serviço externo da Cloudinary que permite que sejam feitos uploads sem a necessidade de sign in.
- Aqui o link para referência ao serviço da Cloudinary https://cloudinary.com/ 
- Documento com orientações sobre os preenchimentos dentro da plataforma da Cloudinary https://docs.google.com/document/d/10viBqwQa_s5-AGoIvg9biqUroSKD8PDExfkHzMTW1fY/edit?tab=t.0 
- Criar uma personalização do tipo texto
- Criar uma tag com o nome `personalizacao-update` com o tipo `personalizacao`
- Adicionar a personalização ao produto

## Comentários

- O objetivo é termos um link para imagem da personalização
- O upload é feito no Cloudinary e isso retorna um link para visualização da imagem
- No lado do Cloudinary as imagens são armazenadas numa pasta (o nome da pasta é designado na plataforma e referenciado no código)
- O script em questão faz a validação do formato do arquivo (opcional), o upload e retorna o link
- Mediante o cenário do seller esse link pode ser inserido de formas diferentes no processo. O processo atual está funcional e envia um atributo personlizado especifico com o valor do link.

- O arquivo `uploadImages` foi desenvolvida para exportar métodos pertinentes ao processo.
  - O método `validateFileType` se basea nos formatos de arquivo no objeto `config` e retorna true ou false baseado nessa validação.
  - O método `validateFileSize` se basea no tamanho de arquivo no objeto `config` e retorna true ou false baseado nessa validação.
  - O método `handleUpload` faz o upload individual da imagem para o Cloudinary.

- No objeto `config`
  - cloudName: É o nome do cloud dentro do serviço do Cloudinary (fornecido pelo seller ou coletado no dashboard do serviço)
  - uploadPreset: É o nome do preset que contém as configurações relacionadas a como o preset vai funcionar, é de extrema importancia que esse item esteja configurado de acordo para que o processo funcione.
  - allowedTypes: Contém os formatos dos arquivos permitidos no upload
