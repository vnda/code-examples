import CartDrawer from '../common/cartDrawer';
import UploadImage from './uploadImages';
import { urlencodeFormData } from './utilities';

const Store = {
  // ..

  addItem: async function (form, parent, callback) {
    const _this = this;
    const btnComprar = parent.querySelector('[data-action="add-cart"]');
    const urlAdd = '/carrinho/adicionar';
    const customizationImage = document.querySelector('[data-customization-image]');

    //////////////////////////////////////////////////////////////////////////////////////////
    //// SESSAO ALTERADA
    //////////////////////////////////////////////////////////////////////////////////////////
    // Efetua o upload antes de adicionar o item ao carrinho
    if (customizationImage) {
      try {
        const link = await UploadImage.handleUpload(window.fileToUpload, customizationImage);
      } catch (error) {
        console.error('Erro ao efetuar o upload para o Cloudinary: ', error);
        return;
      }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //// FIM DA SESSAO ALTERADA
    //////////////////////////////////////////////////////////////////////////////////////////

    const formData = urlencodeFormData(new FormData(form));
    const boxResponse = parent.querySelector('[data-form-product] .msg-response:not(.resp-validate)');

    if (!btnComprar.classList.contains('-adding')) {
      btnComprar.classList.add('-adding');

      try {
        const response = await fetch(urlAdd, {
          method: 'POST',
          headers: {
            'Accept': 'application/json, text/javascript, */*; q=0.0',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
          },
          body: formData,
        });

        const html = await response.text();

        console.log('addItem - success');

        if (typeof callback == 'function') {
          callback('produto-adicionado', html, boxResponse, form);
        } else {
          _this.addItemResult('produto-adicionado', html, boxResponse, form);
        }
      } catch (error) {
        console.log('addItem - error');
        console.error(error);

        if (typeof callback == 'function') {
          callback('erro-adicionar', error, boxResponse, form);
        } else {
          _this.addItemResult('erro-adicionar', error, boxResponse, form);
        }
      }

      btnComprar.classList.remove('-adding');
    }
  },

  // ... 

};

export default Store;
