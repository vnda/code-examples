# BREADCRUMBS

Ajuste os arquivos do seu projeto conforme as orientações abaixo:

**Liquid**: Adicione o arquivo `breadcrumbs.liquid` que está em `liquids` para dentro de `liquids/partials/components`, renderizando-o nas páginas desejadas.

**Estilo**: Adicione o arquivo `breadcrumbs.scss` que está em `assets/sass` para dentro de `assets/sass/components`, importando-o nas páginas desejadas.

&nbsp;

***Como funciona:***

- Cliente acessa uma das páginas em que as `breadcrumbs` estão implementadas;
- Cliente tem que a possibilidade de retroceder para páginas anteriores.

&nbsp;

***Exemplos de uso:***

- Listagem: [Página](https://ethnos.vnda.dev/shop)
- Produto: [Página](https://ethnos.vnda.dev/produto/c16-pe-de-fibra-de-carbono-novo-foot-lorem-ipsum-1)
- Busca: [Página](https://ethnos.vnda.dev/busca?q=produto)
- Página: [Página](https://ethnos.vnda.dev/p/sobre)
- Entrar: [Página](https://ethnos.vnda.dev/entrar)

&nbsp;

***