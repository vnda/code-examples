# LANDING PAGE

Ajuste os arquivos do seu projeto conforme as orientações abaixo:

**Liquids**: 
- Adicione o arquivo `_landing_page.liquid` que está em `liquids/components` para dentro de `liquids/partials/components`;  
- Modifique o arquivo `layout.liquid` do seu projeto com base no que está em `liquids`;

**Estilos**: Adicione o arquivo `_landing_page.scss` que está em `assets/sass` para dentro de `assets/sass/components`;  

&nbsp;

## BANNER

***Informações:***

| Dúvida                          | Instrução                                                                            |
| ------------------------------- | ------------------------------------------------------------------------------------ |
| **Onde cadastrar**              | Banners                                                                              |
| **Onde será exibido**           | Todas as páginas até que o banner seja desativado                                    |

&nbsp;

***Importante***

Com esse modelo será possível acessar as páginas da loja mesmo com o banner ativo, utilizando o parâmetro `lp=false` na URL.

Lembre-se de configurar o arquivo `layout.liquid` corretamente para que o parâmetro funcione conforme o esperado.

&nbsp;

***Orientações sobre os campos:***

| Campo         | Funcional?          | Orientação                                             |
| ------------- | ------------------- | ------------------------------------------------------ |
| **Imagem**    | :large_blue_circle: | Dimensão sugerida: 1920x1080 pixels (desktop) / 800x1700 pixels (mobile). Porém as imagens serão ajustadas de acordo com a resolução da página. |
| **Título**    | :black_circle:      | Alt da imagem.                                         |
| **Subtítulo** | :no_entry:          |                                                        |
| **Descrição** | :no_entry:          |                                                        |
| **Externo?**  | :no_entry:          |                                                        |
| **URL**       | :no_entry:          |                                                        |
| **Posição**   | :black_circle:      | `landing-page` e `landing-page-mobile`                 |
| **Cor**       | :no_entry:          |                                                        |

&nbsp;

***