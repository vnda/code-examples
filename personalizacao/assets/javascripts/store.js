const Store = {

  // Exemplo de função com código completo
  addProduct: function(form, parent, callback) {
    
    // Se tiver customizações, ajusta campos de texto que não foram preenchidos
    const customizations = form.querySelectorAll('[data-customization]');
    const quantity = Number(form.querySelector('[name="quantity"]').value)
    if (customizations.length > 0)
      customizations.forEach(custom => {
        let type = custom.getAttribute('type');
        if (type == 'text')
          if (custom.value === '')
            custom.setAttribute('disabled', true);
      });

    customizations.length > 0 && quantity > 1
      // Item com personalização e com quantidade alta
      ? Store.addItemsCustom(form, parent, callback)
      // Item com quantidade 1, com ou sem personalização
      : Store.addItem(form, parent, callback)
  },

  // Exemplo de função com código completo
  addItemsCustom: async function(form, parent, callback) {
    const _this = this;
    const btnComprar = parent.querySelector('[data-action="add-cart"]');
    const boxResponse = parent.querySelector('[data-form-product] .msg-response:not(.resp-validate)');
    const quantity = Number(form.querySelector('[name="quantity"]').value);
    let hasCustoms = false;
    let customs = {};

    // Prepara o item e suas personalizações
    let data = { items: [
      {
        sku: form.querySelector('[name="sku"]').value,
        quantity,
        customizations: []
      }
    ]};

    const customizations = form.querySelectorAll('[data-customization]');
    if (customizations.length > 0) customizations.forEach(custom => {
      const disabled = custom.getAttribute('disabled')
      let checked = true;
      if (custom.type != 'text')
        if (typeof custom.checked != 'undefined') checked = custom.checked;

      if (checked && disabled !== 'true') {
        hasCustoms = true;
        customs[custom.getAttribute('data-customization-name')] = custom.value;
      }
    })

    if (hasCustoms) {
      for (let index = 0; index < quantity; index++) {
        data.items[0].customizations.push(customs);
      }
    }

    const json_data = JSON.stringify(data);

    // Envia o produto com suas personalizações
    if (!btnComprar.classList.contains('-adding')) {
      btnComprar.classList.add('-adding');

      try {
        const response = await fetch('/carrinho/adicionar/kit', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: json_data
        });
  
        const html = await response.text();

        if (response.ok) {
          if (typeof callback == 'function') {
            callback('produto-adicionado', html, boxResponse, form);
          } else {
            _this.addItemResult('produto-adicionado', html, boxResponse, form);
          }
        } else {
          if (typeof callback == 'function') {
            callback('erro-adicionar', error, boxResponse, form);
          } else {
            _this.addItemResult('erro-adicionar', error, boxResponse, form);
          }
        }
      } catch (error) {
        if (typeof callback == 'function') {
          callback('erro-adicionar', error, boxResponse, form);
        } else {
          _this.addItemResult('erro-adicionar', error, boxResponse, form);
        }
      }

      btnComprar.classList.remove('-adding');

      // Ajusta campos de texto que não foram preenchidos
      const customizations = form.querySelectorAll('[data-customization]');
      if (customizations.length > 0)
        customizations.forEach(custom => {
          let type = custom.getAttribute('type');
          if (type == 'text')
            if (custom.value === '')
              custom.setAttribute('disabled', false);
        });
    }
  },

  // Exemplo de função com código reduzido
  addItemResult: function (form) {
    
    // Se tiver customizações, ajusta campos de texto que não foram preenchidos
    const customizations = form.querySelectorAll('[data-customization]');
    if (customizations.length > 0)
      customizations.forEach(custom => {
        let type = custom.getAttribute('type');
        if (type == 'text')
          if (custom.value === '')
            custom.setAttribute('disabled', false);
      });
  },

};
export default Store;
