import Store from './store';

const ProductPurchase = {

  // Envia o formulário de compra
  submitForm: function (currentProduct) {
    currentProduct.addEventListener('submit', (event) => {
      event.preventDefault();

      const groupShop = currentProduct.closest('[data-product-box]').getAttribute('data-prod-group-shop');
      if (groupShop == null) {

        const form = currentProduct;
        const parent = form.closest('[data-product-box]');

        const respValidated = Store.validateFormProduct(form);
        const boxResponse = parent.querySelector('.msg-response');

        if (respValidated.validated) {
          Store.addProduct(form, parent);
        } else {
          Store.setRespValidateProduct(respValidated, form, boxResponse);
        }
      }
    });
  }
  
};
export default ProductPurchase;
