# PERSONALIZAÇÃO

## ANTES DE COMEÇAR

As instruções deste repositório auxiliam a implementar personalizações nas lojas que não possuem essa estrutura por padrão. Os templates atuais já possuem previsão para personalização neles mesmos.

Você pode confirmar se sua loja já tem previsão para personalizações procurando pelo arquivo `_customizations.liquid` no seu projeto. Se ele já existir, o código da loja já deve ter a estrutura esperada.

Caso contrário, siga os passos abaixo para adicionar a previsão no código.

&nbsp;

## ARQUIVOS

Ajuste os arquivos do seu projeto conforme as orientações abaixo:

**Liquids**:
- Adicione o arquivo `_customizations.liquid` que estão em `liquids/customizations` para dentro de `liquids/partials/content/product/customizations`, adicionando o diretório no projeto;
- Modifique o arquivo `_purchase.liquid` do projeto, em `liquids/partials/content/product`, importando `_customizations.liquid` logo após a importação de `_variants.liquid`;
  - `{% render 'partials/content/product/customizations/customizations', product: product %}`
  - não esqueça da validação de existência de tag, feita através do {% if %}

**Estilos**:
- Adicione o conteúdo de `assets/saas/_attribute.scss` no arquivo de mesmo nome da loja. Incorpore o conteúdo presente nesse arquivo ao que já existe na loja.

**Scripts**:
- Verifique o arquivo `productPurchase.js` com base no que está em `assets/javascripts`, validando se após enviar o formulário com sucesso é invocada a função `addProduct`;
- Verifique o arquivo `store.js` com base no que está em `assets/javascripts`, validando se as funções `addProduct`, `addItemsCustom` e `addItemResult` estão implementadas conforme o esperado;

&nbsp;

***Como funciona:***

- Responsável pela loja cadastra personalizações com base na documentação, vinculando-as em tags que desejar;
- As tags de personalização precisam receber o tipo `personalizacao`
- Responsável pela loja adiciona tags vinculadas em personalizações nos produtos que desejar;
- Cliente acessa um dos produtos, exibindo as personalizações logo após os atributos;
- Cliente adiciona um dos produtos ao carrinho, incluindo as personalizações selecionadas;
- Responsável pela loja fica encarregado de avaliar os pedidos recebidos, personalizando os produtos conforme o esperado.

&nbsp;

***Importante:***

- Para habilitar as personalizações no painel administrativo da loja, vá em Configurações > Extras e marque a opção `Habilitar cadastro e configuração de produtos personalizados`;
- Com esse modelo será necessário cadastrar as personalizações no painel administrativo da loja acessando `/admin/personalizados`;
- Acesse o painel administrativo da loja e cadastre personalizações de tipo `Opções` e/ou `Texto`, vinculando-as em tags que desejar;
- Produtos com tags de personalizações exibirão as opções de personalização na página interna;
- Os arquivos disponibilizados nesse modelo são apenas exemplos de uso, incluindo somente os trechos de código necessários para a implementação;
- Adapte os arquivos conforme as necessidades do projeto, incluindo/modificando os trechos de código apresentados;
- Adapte as personalizações de tipo `Opções` para que sejam exibidas em elementos `input` e `label` conforme as necessidades do projeto;
- Adapte as personalizações de tipo `Texto` para que sejam exibidas em elementos `input` com `type="text"` conforme as necessidades do projeto.

&nbsp;

***Exemplo de uso:***

- Produto com personalização: [Página](https://pjlovers.vnda.dev/produto/ping-frame-shiny-moon-1) / [Admin](https://pjlovers.vnda.dev/admin/produtos/editar?id=1)
- Personalização de tipo `Opções`: [Admin](https://pjlovers.vnda.dev/admin/personalizados/editar?id=2)
- Personalização de tipo `Texto`: [Admin](https://pjlovers.vnda.dev/admin/personalizados/editar?id=1)
- Tag das personalizações: [Admin](https://pjlovers.vnda.dev/admin/tags/editar?id=personalizado)

&nbsp;

## PERSONALIZAÇÃO OPÇÕES

***Informações:***

| Dúvida                | Instrução                              |
| ----------------------| -------------------------------------- |
| **Onde cadastrar**    | Personalizados `/admin/personalizados` |
| **Onde será exibido** | Página interna do produto              |

***Orientações sobre os campos:***

| Campo                    | Funcional?          | Orientação                                       |
| ------------------------ | ------------------- | ------------------------------------------------ |
| **Imagem**               | :no_entry:          |                                                  |
| **Tipo do grupo**        | :black_circle:      | `Opções`                                         |
| **Nome do grupo**        | :black_circle:      | Nome do grupo, exibido logo acima dos seletores. |
| **Rótulo**               | :black_circle:      | Texto que será exibido na opção do seletor.      |
| **SKU**                  | :black_circle:      | Obrigatório no admin, sem reflexo na página.     |
| **Nome**                 | :black_circle:      | Texto que será exibido no seletor.               |
| **Preço**                | :large_blue_circle: | Preço da personalização.                         |
| **Quantidade**           | :large_blue_circle: | Quantidade disponível.                           |
| **Dias para preparação** | :large_blue_circle: | Dias necessários para a preparação.              |
| **Tag**                  | :black_circle:      | Tag cuja personalização estará vinculada. Adicionar a tag aos produtos que possuirão a personalização. |

&nbsp;

## PERSONALIZAÇÃO TEXTO

***Informações:***

| Dúvida                | Instrução                              |
| ----------------------| -------------------------------------- |
| **Onde cadastrar**    | Personalizados `/admin/personalizados` |
| **Onde será exibido** | Página interna do produto              |

***Orientações sobre os campos:***

| Campo                    | Funcional?          | Orientação                                           |
| ------------------------ | ------------------- | ---------------------------------------------------- |
| **Imagem**               | :no_entry:          |                                                      |
| **Tipo do grupo**        | :black_circle:      | `Texto`                                              |
| **Nome do grupo**        | :black_circle:      | Nome do grupo, exibido logo acima do campo de texto. |
| **Formato**              | :black_circle:      | Selecionar `Qualquer texto`.                         |
| **Rótulo**               | :black_circle:      | Obrigatório no admin, sem reflexo na página.         |
| **SKU**                  | :black_circle:      | Obrigatório no admin, sem reflexo na página.         |
| **Preço**                | :large_blue_circle: | Preço da personalização.                             |
| **Quantidade**           | :large_blue_circle: | Quantidade disponível.                               |
| **Dias para preparação** | :large_blue_circle: | Dias necessários para a preparação.                  |
| **Tag**                  | :black_circle:      | Tag cuja personalização estará vinculada. Adicionar a tag aos produtos que possuirão a personalização. |

&nbsp;

***