# COR POR REFERÊNCIA

Ajuste os arquivos do seu projeto conforme as orientações abaixo:

**Liquid**: Modifique o arquivo `attributes.liquid` com base no que está em `liquids`, incluindo as verificações da tag de tipo `modelo`.

**Estilo**: Modifique o arquivo `attributes.scss` com base no que está em `assets/sass`, incluindo as estilizações para atributos com link.

&nbsp;

***Como funciona:***

- Responsável pela loja cadastra e adiciona tags `modelo` nos produtos que desejar, formando diferentes grupos para cada tag cadastrada;
- Cliente acessa um dos produtos, exibindo as cores dos demais produtos vinculados via tag para seleção;
- Cliente seleciona uma das cores, sendo redirecionado para a página do produto selecionado.

&nbsp;

***Importante:***

- Com esse modelo será necessário cadastrar a(s) tag(s) de tipo `modelo` no painel administrativo da loja acessando `/admin/tags`;
- Acesse o painel administrativo da loja e cadastre uma tag preenchendo o campo `Tipo` com `modelo`, vinculando-a aos produtos na sequência;
- Produtos com a tag habilitarão a seleção de cor por referência nos atributos, redirecionando o cliente para o produto selecionado;
- Os arquivos disponibilizados nesse modelo são apenas exemplos de uso, incluindo somente os trechos de código necessários para a implementação;
- Adapte os arquivos conforme as necessidades do projeto, incluindo os trechos de código apresentados.

&nbsp;

***Exemplos de uso:***

- Produto com tag de tipo `modelo`: [Página](https://afora.vnda.dev/produto/vestido-vintage-framboesa-1) / [Admin](https://afora.vnda.dev/admin/produtos/editar?id=1)
- Produtos com tag de tipo `modelo`: [Admin](https://afora.vnda.dev/admin/produtos?page=1&perPage=50&sortBy=updated_at&orderBy=desc&q=&tags=modelo-exemplo&status=active)
- Tag de tipo `modelo`: [Admin](https://afora.vnda.dev/admin/tags/editar?id=modelo-exemplo)

&nbsp;

## TAGS

***Informações:***

| Dúvida                          | Instrução                   |
| ------------------------------- | --------------------------- |
| **Onde cadastrar**              | Tags `/admin/tags`          |
| **Onde será exibido**           | Seletor de cores do produto |

***Informações sobre os campos***

| Campo         | Funcional?          | Orientação                                   |
| ------------- | ------------------- | -------------------------------------------- |
| **Nome**      | :black_circle:      | Obrigatório no admin, sem reflexo na página. |
| **Título**    | :no_entry:          |                                              |
| **Subtítulo** | :no_entry:          |                                              |
| **Descrição** | :no_entry:          |                                              |
| **Tipo**      | :black_circle:      | `modelo`                                     |
| **Imagem**    | :no_entry:          |                                              |

&nbsp;

***