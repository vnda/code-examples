# B2B ORÇAMENTO

Ajuste os arquivos do seu projeto conforme as orientações abaixo:

**Liquids**:
- Adicione o arquivo `_cart_form.liquid` que está em `liquids/common` para dentro de `liquids/partials/common`;
- Adicione o arquivo `cart_popup.liquid` que está em `liquids` para dentro de `liquids`, substituindo o arquivo padrão do projeto;
- Modifique o arquivo `layout.liquid` com base no que está em `liquids`, incluindo a chamada ao `cart_form`.

**Estilos**:
- Modifique o arquivo `_cart_drawer.scss` com base no que está em `assets/sass`, incluindo a estilização do formulário e de preços ocultos.

**Scripts**:
- Modifique o arquivo `cartDrawer.js` com base no que está em `assets/javascripts`, incluindo a função `budget` para controle do formulário de orçamento e de preços ocultos;
- Modifique o arquivo `webforms.js` com base no que está em `assets/javascripts`, incluindo a remoção de itens do carrinho após envio e o controle de formulários inicializados.

&nbsp;

***Como funciona:***

- Responsável pela loja adiciona tags `orcamento` ou `orcamento-sem-preco` nos produtos que desejar;
- Cliente adiciona um dos produtos ao carrinho, habilitando o formulário de orçamento;
- Cliente preenche os campos do formulário e envia para validação;
- Os dados são enviados para o endereço de e-mail vinculado através do painel;
- Os itens adicionados ao carrinho são removidos após envio do formulário;
- Responsável pela loja fica encarregado de avaliar os orçamentos recebidos via e-mail e entrar em contato com os clientes.

&nbsp;

***Importante:***

- Será necessário cadastrar o formulário no painel administrativo da loja acessando `/admin/config/mensagens-e-avisos/forms`;
- Acesse o painel administrativo da loja e cadastre preenchendo `Assunto` com `Orçamento`, resultando na chave `nome_da_loja-orcamento`;
- Ao enviar o formulário serão enviadas as informações de orçamento para o e-mail vinculado;
- Será necessário cadastrar as tags `orcamento` e `orcamento-sem-preco` no painel administrativo da loja acessando `/admin/tags`;
- Acesse o painel administrativo da loja e cadastre preenchendo `Nome` com `orcamento` ou `orcamento-sem-preco` e `Tipo` com `comportamento-de-carrinho`;
- Produtos com a tag `orcamento` habilitarão o formulário de orçamento quando adicionado ao carrinho;
- Produtos com a tag `orcamento-sem-preco` habilitarão o formulário de orçamento e não exibirão seu valor quando adicionados ao carrinho;
- Verificando `product.tags` no liquid é possível controlar a exibição de preço em outras etapas da loja.

&nbsp;

***Exemplos de uso:***

- Produto com tag `orcamento`: [Página](https://casadealessa.vnda.dev/produto/poltrona-baixa-gg-orcamento-2) / [Admin](https://casadealessa.vnda.dev/admin/produtos/editar?id=2)
- Produto com tag `orcamento-sem-preco`: [Página](https://casadealessa.vnda.dev/produto/poltrona-baixa-gg-sem-preco-3) / [Admin](https://casadealessa.vnda.dev/admin/produtos/editar?id=3)
- Tag `orcamento`: [Admin](https://casadealessa.vnda.dev/admin/tags/editar?id=orcamento)
- Tag `orcamento-sem-preco`: [Admin](https://casadealessa.vnda.dev/admin/tags/editar?id=orcamento-sem-preco)
- Formulário `Orçamento`: [Admin](https://casadealessa.vnda.dev/admin/config/mensagens-e-avisos/forms/editar?id=6)

&nbsp;

## FORMULÁRIO

***Informações:***

| Dúvida                          | Instrução                                            |
| ------------------------------- | ---------------------------------------------------- |
| **Onde cadastrar**              | Formulários `/admin/config/mensagens-e-avisos/forms` |
| **Onde será exibido**           | Componente de carrinho                               |

***Orientações sobre os campos:***

| Campo            | Funcional?     | Orientação |
| ---------------- | -------------- | ---------- |
| **Destinatário** | :black_circle: | Endereço de e-mail para qual as informações de novos orçamentos serão enviadas. |
| **Assunto**      | :black_circle: | `Orçamento` |

&nbsp;

## TAGS

***Informações:***

| Dúvida                          | Instrução              |
| ------------------------------- | ---------------------- |
| **Onde cadastrar**              | Tags `/admin/tags`     |
| **Onde será exibido**           | Componente de carrinho |

***Informações sobre os campos***

| Campo         | Funcional?          | Orientação                                           |
| ------------- | ------------------- | ---------------------------------------------------- |
| **Nome**      | :black_circle:      | `orcamento` ou `orcamento-sem-preco`                 |
| **Título**    | :no_entry:          |                                                      |
| **Subtítulo** | :no_entry:          |                                                      |
| **Descrição** | :no_entry:          |                                                      |
| **Tipo**      | :black_circle:      | `comportamento-de-carrinho`                          |
| **Imagem**    | :no_entry:          |                                                      |

&nbsp;

***