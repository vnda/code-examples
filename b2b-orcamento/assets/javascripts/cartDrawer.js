import Webforms from './webforms';

const CartDrawer = {
  
  // Função para controle do formulário de orçamento no carrinho
  budget: function () {
    const root = document.querySelector('#component-cart-drawer-root'),
          selectorItem = '.cart-item',
          selectorName = '.cart-list-item-name a',
          selectorPrice = '.cart-list-item-price',
          selectorAmount = '.cart-buttons-amount'

    if (root) {
      window.lastAmount = 0;

      // To reset empty cart
      let reset = () => {
        window.lastAmount = 0;
        if (root.querySelector('[data-cart-form]'))
          root.querySelector('[data-cart-form]').remove();
      }

      // Create cart observer
      let observer = new MutationObserver(() => {

        // Get amount elements
        let amounts = root.querySelectorAll(selectorAmount);
        if (!amounts.length) return reset();

        // Set current amount
        let currentAmount = 0;
        amounts.forEach(amount => { currentAmount += Number(amount.innerText) });
        
        // Verify last amount
        if (window.lastAmount != currentAmount) {
          window.lastAmount = currentAmount;

          // Get cart json
          vnda.loadCartPopup(async (json) => {
            let cart = await JSON.parse(json);

            // Get items element & items json
            let itemsElement = root.querySelectorAll(selectorItem);
            let itemsJson = cart.items;
            if (!itemsElement.length || !itemsJson.length) return;

            // Iterate items
            let budgetTags = false;
            let budgetProducts = '';
            itemsElement.forEach(itemElement => {
              let name = itemElement.querySelector(selectorName).innerText;
              let amount = itemElement.querySelector(selectorAmount).innerText;

              // Save item name
              budgetProducts +=
                budgetProducts == ''
                  ? `${name}: ${amount}`
                  : ` | ${name}: ${amount}`

              // Verify item name & item tags
              itemsJson.forEach(itemJson => {
                if (itemJson.product_name == name && itemJson.tags.length)
                  
                  // Verify item tags
                  itemJson.tags.forEach(tag => {

                    // Verify budget tags
                    if (tag.name.includes('orcamento'))
                      budgetTags = true;

                    // Verify no price tag
                    if (tag.name == 'orcamento-sem-preco') {

                      // Remove item price
                      itemElement.querySelector(selectorPrice).classList.add('-hidden');
                      itemElement.querySelector(selectorPrice).innerHTML = 0;
                    }
                  });
              });
            });

            // Verify budget tags
            if (budgetTags) {

              // Get form & wrapper
              let form = document.querySelector('[data-cart-form]');
              let wrapper = root.querySelector(selectorItem).closest('.cart-drawer-relative');
              if (form && wrapper) {
                if (wrapper.querySelector('[data-cart-form]'))
                  wrapper.querySelector('[data-cart-form]').remove();

                // Clone form
                let newForm = form.cloneNode(true);

                // Update input
                newForm.querySelector('input[name="products"]').value = budgetProducts;

                // Append form
                newForm.classList.remove('-set');
                wrapper.insertBefore(newForm, wrapper.lastChild);
                Webforms.init();
              }

            // Remove form
            } else
              if (root.querySelector('[data-cart-form]'))
                root.querySelector('[data-cart-form]').remove();
          });
        }
      });

      // Initialize cart observer
      observer.observe(root, { childList: true, subtree: true });
    }
  },

  init: function() {
    this.budget();
  }
};
export default CartDrawer;
