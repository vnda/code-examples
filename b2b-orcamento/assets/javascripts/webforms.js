const Webforms = {

  // Função adaptada para remover itens do carrinho após envio
  submitWebForm: async function(form) {
    if (!form.classList.contains('-sending')) {
      form.classList.add('-sending');

      try {
        const response = await fetch('/webform', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
          },
          body: formData
        });

        if (response.ok) {
          form.classList.add('-sent');
          form.querySelector('.msg-success').classList.add('-visible');
          form.reset();

          setTimeout(() => {
            form.classList.remove('-sent');
            form.querySelector('.msg-success').classList.remove('-visible');

            // Remove itens do carrinho após envio
            if (form.getAttribute('name') == 'orcamento') {
              let removes = document.querySelectorAll('.cart-buttons-trash');
              if (removes.length > 0)
                removes.forEach(remove => { remove.click() });
            }

          }, 3500);
        }
      } catch (error) {
        console.error(error);
      }
      
      form.classList.remove('-sending');
    }
  },

  // Função adaptada para o formulário de orçamento
  setWebForms: function () {    

    // Seleciona apenas os formulários que ainda não foram inicializados
    const webForms = document.querySelectorAll('[data-webform]:not(.-set)');
    webForms.length > 0 && webForms.forEach(form => {
      
      // Adiciona uma classe ao formulário para impedir múltiplas inicializações
      form.classList.add('-set');
    });

  },

  init: function () {
    this.setWebForms();
  }
};
export default Webforms;
