# Code Examples Vnda

Repositório com exemplos de código de referências para parceiros.

- [Assinatura de produtos](https://gitlab.com/vnda/code-examples/-/tree/main/assinatura)
- [B2B: Cadastro de clientes](https://gitlab.com/vnda/code-examples/-/tree/main/b2b-cadastro)
- [B2B: Clientes com tags](https://gitlab.com/vnda/code-examples/-/tree/main/b2b-cliente)
- [B2B: Formulário de orçamento](https://gitlab.com/vnda/code-examples/-/tree/main/b2b-orcamento)
- [Blog](https://gitlab.com/vnda/code-examples/-/tree/main/blog)
- [Breadcrumbs](https://gitlab.com/vnda/code-examples/-/tree/main/breadcrumbs)
- [Busca reativa](https://gitlab.com/vnda/code-examples/-/tree/main/busca-reativa)
- [Dados do cliente](https://gitlab.com/vnda/code-examples/-/tree/main/dados%20do%20cliente%20logado)
- [Cor por referência](https://gitlab.com/vnda/code-examples/-/tree/main/cor-por-referencia)
- [Descrição com abas](https://gitlab.com/vnda/code-examples/-/tree/main/descricao-com-abas)
- [Landing page](https://gitlab.com/vnda/code-examples/-/tree/main/landing-page)
- [Landing page (localStorage)](https://gitlab.com/vnda/code-examples/-/tree/main/landing-page-localstorage)
- [Locals](https://gitlab.com/vnda/code-examples/-/tree/main/locals)
- [Personalização](https://gitlab.com/vnda/code-examples/-/tree/main/personalizacao)
- [Trustvox](https://gitlab.com/vnda/code-examples/-/tree/main/trustvox)
- [Vídeo: HTML5](https://gitlab.com/vnda/code-examples/-/tree/main/video-html5)
- [Vídeo: Vimeo/YouTube](https://gitlab.com/vnda/code-examples/-/tree/main/video-iframe-vimeo-youtube)