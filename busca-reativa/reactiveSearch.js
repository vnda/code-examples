// Importar esse arquivo no main.js

import { formatMoney } from '../components/utilities';

const ReactiveSearch = {
    handleAutocomplete: async function(_term){
      const headers = new Headers();
      headers.append('accept', 'application/json');  
    
      const requestInit = { headers: headers }    
      const response = await fetch(`/busca?q=${_term}`, requestInit);  
      this.handlePopulate(await response.json());
    },
    handlePopulate: function(_response){
      let searchResult = "";
      const resultContainer = document.querySelector('[data-search-result]');
  
      if (_response.length > 0) {
       _response.forEach((product)=>{
         searchResult += `
          <div class="product-item">
            <div class="product-image">
              <img src="${product.image_url}">
            </div>
            <div class="product-text">
              <h3>${product.name}</h3>
              <span>${formatMoney(product.price)}</span>
            </div>
          </div>
        `
       })      
       resultContainer.innerHTML = searchResult;
     } else {
       resultContainer.innerHTML = "";
     }
    },
    init: function(){
      const searchInput = document.querySelector('.search-form input');
      var searchtimer;

      searchInput.addEventListener('input', (e)=>{
        clearTimeout(searchtimer);
        var tempInput = e.currentTarget.value;
        searchtimer = setTimeout(() => {
          this.handleAutocomplete(tempInput);
        }, 1000);
      })
    }
  }
  
  export default ReactiveSearch;
