import { serializeArray } from "../components/utilities";

const LoginPage = {

  initForms: function () {
    const
      inputCNPJ = document.querySelector('input[name="cnpj"]'),
      inputCEP = document.querySelector('input[name="cep"]'),
      inputPhone = document.querySelectorAll('input[name="telephone"]'),
      buttonLogin = document.querySelector('[data-form-login] button[type="button"]'),
      buttonRecover = document.querySelector('[data-form-recover] button[type="button"]'),
      buttonsRegister = document.querySelectorAll('[data-form-register] button[type="button"]')

    if (inputPhone)
      inputPhone.forEach(input => {
        input.addEventListener('input', function (e) {
          var phoneValue = e.target.value.replace(/\D/g, '').match(/(\d{0,2})(\d{0,5})(\d{0,4})/);
          e.target.value = !phoneValue[2] ? phoneValue[1] : '(' + phoneValue[1] + ') ' + phoneValue[2] + (phoneValue[3] ? '-' + phoneValue[3] : '');
        });
      });


    if (inputCNPJ)
      inputCNPJ.addEventListener('input', function (e) {
        let v = e.target.value.replace(/\D/g, '');
        v = v.replace(/^(\d{2})(\d)/, '$1.$2');
        v = v.replace(/^(\d{2})\.(\d{3})(\d)/, '$1.$2.$3');
        v = v.replace(/\.(\d{3})(\d)/, '.$1/$2');
        v = v.replace(/(\d{4})(\d)/, '$1-$2');  
        e.target.value = v;
      });

    if (inputCEP)
      inputCEP.addEventListener('input', function (e) {
        let x = e.target.value.replace(/\D/g, '').match(/(\d{0,5})(\d{0,3})/);
        e.target.value = !x[2] ? x[1] : x[1] + '-' + x[2];
      });
    
    if (buttonLogin)
      buttonLogin.addEventListener('click', (e) => {
        e.preventDefault();
        if (LoginPage.validateForms(e.target.form))
          LoginPage.login(e.target.form);
      });

    if (buttonRecover)
      buttonRecover.addEventListener('click', (e) => {
        e.preventDefault();
        if (LoginPage.validateForms(e.target.form))
          LoginPage.recover(e.target.form);
      });

    if (buttonsRegister.length)
      buttonsRegister.forEach(buttonRegister => {
        buttonRegister.addEventListener('click', (e) => {
          e.preventDefault();
          if (LoginPage.validateForms(e.target.form))
            LoginPage.login(e.target.form);
        });
      });
  },
  toggleForms: function () {
    const
      login = document.querySelector('[data-form-login]'),
      recover = document.querySelector('[data-form-recover]')
    if (login && recover) {
      
      let buttonLogin = login.querySelector('.anchor');
      let buttonRecover = recover.querySelector('.anchor');
      if (buttonLogin && buttonRecover) {

        buttonLogin.addEventListener('click', (e) => {
          e.preventDefault();
          login.classList.remove('-active');
          recover.classList.add('-active');
        });
        buttonRecover.addEventListener('click', (e) => {
          e.preventDefault();
          recover.classList.remove('-active');
          login.classList.add('-active');
        });
      }
    }
  },
  validateForms: function (form) {
    let valid = true;

    let inputs = form.querySelectorAll('input[required]');
    if (inputs.length > 0)
      inputs.forEach(input => {
        if (input.name != 'vnda') {
          if (input.type == 'text' || input.type == 'email' || input.type == 'password')
            if (input.value == '')
              valid = false;
          if (input.type == 'checkbox')
            if (!input.checked)
              valid = false;
        }
      });
    
    let select = form.querySelector('select[required]');
    if (select && select.selectedIndex == 0)
      valid = false;

    if (!valid)
      LoginPage.showErrors(form);

    return valid;
  },
  showErrors: function (form, error) {
    let msgReturn = form.querySelector('.msg-return');

    msgReturn.innerHTML = `<p>${
      error
        ? `${error}.`
        : 'Preencha todos os campos!'
    }</p>`;
    msgReturn.classList.add('-active');
    
    setTimeout(() => {
      msgReturn.classList.remove('-active');
      msgReturn.innerHTML = '';
    }, 3000);
  },

  login: function (form) {

    // Manipulando o campo de telefone
    const telefone = form.getElementById('register-phone').value;
    const phoneArea = telefone.substring(1, 3);
    const phone = telefone.substring(5, 16).replace(/\D/g, '');

    formData.append("phone_area", phoneArea);
    formData.append("phone", phone);

    if (!form.classList.contains('-sending-login'))
      $.ajax({
        url: '/entrar',
        type: 
          form.hasAttribute('data-form-register') 
            ? 'POST' 
            : 'PUT',
        dataType: 'json',
        data: $(form).serialize(),
        beforeSend: function () {
          form.classList.add('-sending-login');
          if (form.querySelector('[data-default]'))
            form.querySelector('[data-default]').innerHTML = 'ENVIANDO...';
        }
      })
        .done(async function (resp) {
          if (resp.error) {
            LoginPage.showErrors(form, resp.error);

            if (form.querySelector('[data-default]'))
              form.querySelector('[data-default]').innerHTML = form.querySelector('[data-default]').getAttribute('data-default');
            form.classList.remove('-sending-login');

          } else if (resp.client) {
            if (form.hasAttribute('data-form-register')) {

              // Desabilita campos de senha antes de enviar
              let passwords = form.querySelectorAll('input[type="password"]');
              passwords.forEach(password => { password.disabled = true });

              // Envia webform antes de redirecionar
              await fetch('/webform', {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                },
                body: serializeArray(form)
              });
            }

            if (form.querySelector('[data-default]'))
              form.querySelector('[data-default]').innerHTML = form.querySelector('[data-default]').getAttribute('data-default');
            window.location = '/conta';
          }
        })
        .fail(function (resp) {
          console.error('error', resp);

          if (form.querySelector('[data-default]'))
            form.querySelector('[data-default]').innerHTML = form.querySelector('[data-default]').getAttribute('data-default');
          form.classList.remove('-sending-login');
        })
  },
  recover: function (form) {
    if (!form.classList.contains('-sending'))
      $.ajax({
        url: '/recuperar_senha',
        type: 'POST',
        dataType: 'json',
        data: $(form).serialize(),
        beforeSend: function () {
          form.classList.add('-sending');
        }
      })
        .done(function (resp) {
          if (resp.error) LoginPage.showErrors(form, resp.error);
          if (resp.ok) LoginPage.showErrors(form, resp.ok);
        })
        .fail(function (resp) {
          console.error('error', resp);
        })
        .always(function () {
          form.classList.remove('-sending');
        })
  },

  init: function () {
    this.initForms();
    this.toggleForms();
  },
};

window.addEventListener('DOMContentLoaded', () => {
  LoginPage.init()
})
