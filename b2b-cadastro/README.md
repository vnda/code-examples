# B2B CADASTRO

Ajuste os arquivos do seu projeto conforme as orientações abaixo:

**Liquid**: Adicione o arquivo `login.liquid` que está em `liquids` para dentro de `liquids`, substituindo o arquivo padrão do projeto.

**Estilo**: Adicione o arquivo `login_above.scss` que está em `assets/sass` para dentro de `assets/sass/pages`, substituindo o arquivo padrão do projeto.

**Script**: Adicione o arquivo `login_page.js` que está em `assets/javascripts` para dentro de `assets/javascripts/pages`, substituindo o arquivo padrão do projeto.

&nbsp;

***Como funciona:***

- Cliente preenche os campos do formulário para `Cadastro` ou `Cadastro Empresarial` e envia para validação;
- Após validação, caso os dados sejam válidos o cliente é cadastrado na plataforma;
- Após cadastro na plataforma, os dados são enviados para o endereço de e-mail vinculado através do painel;
- Após envio dos dados, cliente é redirecionado para `/conta`;
- Responsável pela loja fica encarregado de avaliar os dados recebidos via e-mail e vincular tags de acesso ao cliente.

&nbsp;

***Importante:***

- Com esse modelo será necessário cadastrar os formulários no painel administrativo da loja acessando `/admin/config/mensagens-e-avisos/forms`;
- Dois cadastros serão obrigatórios para o recebimento de informações, visto que nesse exemplo há um formulário para `Cadastro` e outro para `Cadastro Empresarial`;
- Acesse o painel administrativo da loja e cadastre o primeiro preenchendo `Assunto` com `Cadastro`, resultando na chave `nome_da_loja-cadastro`;
- Acesse o painel administrativo da loja e cadastre o segundo preenchendo `Assunto` com `Cadastro Empresarial`, resultando na chave `nome_da_loja-cadastro-empresarial`;
- Ao cadastrar um cliente pela página serão enviadas também as informações de cadastro para o e-mail vinculado aos formulários.

&nbsp;

***Exemplos de uso:***

- Formulário `Cadastro`: [Página](https://ethnos.vnda.dev/entrar) / [Admin](https://ethnos.vnda.dev/admin/config/mensagens-e-avisos/forms/editar?id=1)
- Formulário `Cadastro Empresarial`: [Página](https://ethnos.vnda.dev/entrar) / [Admin](https://ethnos.vnda.dev/admin/config/mensagens-e-avisos/forms/editar?id=2)

&nbsp;

## FORMULÁRIOS

***Informações:***

| Dúvida                          | Instrução                                            |
| ------------------------------- | ---------------------------------------------------- |
| **Onde cadastrar**              | Formulários `/admin/config/mensagens-e-avisos/forms` |
| **Onde será exibido**           | Página `Entrar`                                      |

***Orientações sobre os campos:***

| Campo            | Funcional?     | Orientação |
| ---------------- | -------------- | ---------- |
| **Destinatário** | :black_circle: | Endereço de e-mail para qual as informações de cadastro de novos clientes serão enviadas. |
| **Assunto**      | :black_circle: | `Cadastro` e/ou `Cadastro Empresarial` (conforme os requisitos da loja) |

&nbsp;

***