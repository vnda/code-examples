# TRUSTVOX

Movimente os trechos indicados deste repositório para o seu projeto conforme as orientações abaixo:

**Liquids**: cada arquivo contém a indicação do *trecho* de código necessário e o local onde o mesmo deve ser adicionado à loja;

- `layout.liquid`: deve receber as configurações base da Trustvox - trechos em Javascript;
- `home.liquid`: deve receber os trechos Javascript e HTML para a exibição do carrossel com avaliações da loja;
- `product.liquid`: deve receber os trechos Javascript e HTML para a exibição das avaliações com estrelas e campo para relatos dos usuários;
- `product_block.liquid`: deve receber o trecho HTML para a exibição das avaliações com estrelas do produto;

**Estilos**:
- Estilizar os elementos da Trustvox como desejar;

**Scripts**:
- Adicione a função `setTrustvox` no arquivo `assets/javascripts/utilities.js` do seu projeto;
- Importe e a execute no arquivo `main.js`

## Comentários

- A função `setTrustvox` foi desenvolvida considerando a performance da loja. Os scripts só serão inseridos de acordo com alguma interação do usuário com as páginas;
- As informações dos produtos precisam estar previamente cadastradas no painel da Trustvox;
- Documentação oficial: https://help.trustvox.com.br/pt-BR/articles/5557705-como-exibir-as-opinioes-e-adicionar-os-scripts-da-ra-trustvox-de-forma-generica


&nbsp;