// ===============================================================
// INSERÇÃO CONDICIONAL DO SCRIPT DE AVALIAÇÕES TRUSTVOX
// ===============================================================

// Importar a função e executar no main.js

export function setTrustvox() {
  // Avaliações - Script Geral
  const trustvoxRateScript = document.createElement('script')
  trustvoxRateScript.setAttribute('src', '//rate.trustvox.com.br/widget.js');
  trustvoxRateScript.setAttribute('async', 'true');
  trustvoxRateScript.setAttribute('type', 'text/javascript');

  // Campo para avaliação individual do produto - Usuários enviam relato - Exibido na página transacional
  const sinceroScript = document.createElement('script')
  sinceroScript.setAttribute('src', '//static.trustvox.com.br/sincero/sincero.js');
  sinceroScript.setAttribute('async', 'true');
  sinceroScript.setAttribute('type', 'text/javascript');

  // Avaliações individual do produto - Usuários enviam relato - Exibido na página transacional
  const sinceroShopScript = document.createElement('script')
  sinceroShopScript.setAttribute('src', '//static.trustvox.com.br/sincero/sincero.js');
  sinceroShopScript.setAttribute('async', 'true');
  sinceroShopScript.setAttribute('type', 'text/javascript');

  // Avaliações da loja - Carrossel - Exibidas na Home
  const coltScript = document.createElement('script')
  coltScript.setAttribute('src', '//colt.trustvox.com.br/colt.min.js');
  coltScript.setAttribute('type', 'text/javascript');

  const eventType = (window.innerWidth <= 1024) ? 'scroll' : 'mousemove'
  window.addEventListener(eventType, () => {

    // Geral
    document.head.appendChild(trustvoxRateScript);

    const page = document.body.getAttribute('data-page');

    // Insere somente na página de produto - Avaliações individuais do produto
    if (page == 'product') {
      document.head.appendChild(sinceroScript);
    }

    // Insere as avalições da loja somente na Home
    if (page == 'home') {
      document.body.appendChild(coltScript);
    }

  }, { once: true })
}